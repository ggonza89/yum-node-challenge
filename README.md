# yum-node-challenge

## Usage

Divvy Bike Rental API.

## Running app via Docker image

If you don't already have a docker hub account and docker installed locally this will provide steps on how to get it up and running.

- Install Docker Desktop: https://www.docker.com/get-started
- Create a Docker Hub account: https://hub.docker.com/
- Once completed you can see that docker is installed by running `docker -v`
- Go ahead and login with your docker hub credentials by running `docker login`
- To pull down the image run the command `docker pull ggonza89/yum-node-challenge:v1.0.0`
- To run the image against port 5000 on your local machine so that the tests will pass run the command `docker run -p 5000:3000 ggonza89/yum-node-challenge:v1.0.0`

Feel free to start using the API once you have seen `listening at http://localhost:3000` in the console output to be sure it is ready to serve.

## Authentication

An API token is needed in order to use the API. It can be anything as long as the cookie exists.

### Axios authentication

Using axios you can pass an API token using the config headers cookie property.

Here is an example call to getStationById retrieving station with id of 2:

```
await axios.post(
  `http://localhost:3000/getStationById`,
  { id: 2 },
  {
      headers: {
          Cookie: 'accessToken=1234'
      }
  }
)
```

### Postman authentication

Make sure to use a POST request.

Navigate to the Headers section and add the Cookie param of `accessToken` equal to whatever you'd like.

Pass parameters through body using a raw JSON type like so:

```
{
   "date": "04/01/2019",
   "stationIds": [56]
}
```

## API definition

### 1. Get Station By Id

URL: http://localhost:3000/getStationById

Required parameters: `id`: station id

Example Response:
```
{
    "rental_uris": {
        "ios": "https://chi.lft.to/lastmile_qr_scan",
        "android": "https://chi.lft.to/lastmile_qr_scan"
    },
    "has_kiosk": true,
    "short_name": "15541",
    "capacity": 39,
    "electric_bike_surcharge_waiver": false,
    "station_id": "2",
    "external_id": "a3a36d9e-a135-11e9-9cda-0a87ae2ba916",
    "eightd_has_key_dispenser": false,
    "lat": 41.87651122881695,
    "name": "Buckingham Fountain",
    "eightd_station_services": [],
    "rental_methods": [
        "CREDITCARD",
        "KEY",
        "TRANSITCARD"
    ],
    "station_type": "classic",
    "lon": -87.62054800987242
}
```
400: Most likely missing the id parameter.  You should see the error message in the response

404: The id passed in was not found amongst the divvy data.

200: The station with the id provided will be returned in response.

### 2. Riders on given day

URL: http://localhost:3000/ridersOnGivenDay

Required parameters: 
    `date`: date string in MM/DD/YYYY format
    `stationIds`: an array of integers with valid station ids
    
Response:

```
[
    {
        "ageGroup": "0-20",
        "riders": 0
    },
    {
        "ageGroup": "21-30",
        "riders": 18
    },
    {
        "ageGroup": "31-40",
        "riders": 17
    },
    {
        "ageGroup": "41-50",
        "riders": 5
    },
    {
        "ageGroup": "51+",
        "riders": 2
    },
    {
        "ageGroup": "unknown",
        "riders": 1
    }
]
```


400: Most likely missing a parameter.  You should see the error message in the response along with what parameter is missing

200: You will get a list of objects with ageGroup and riders count as properties.

### 3. Last 20 trips on given day and station ids

URL: http://localhost:3000/lastTrips

Required parameters: 
    `date`: date string in MM/DD/YYYY format
    `stationIds`: an array of integers with valid station ids
    
Response:

```
{
  "56": [
    {
      localStartTime: "2019-04-01 00:02:22",
      localEndTime: "2019-04-01 00:09:48",
      endStationId: 56,
      birthYear: 1975,
    },
    {
      localStartTime: "2019-04-01 00:03:02",
      localEndTime: "2019-04-01 00:20:30",
      endStationId: 56,
      birthYear: 1984,
    },
    {
      localStartTime: "2019-04-01 00:02:22",
      localEndTime: "2019-04-01 17:09:48",
      endStationId: 56,
      birthYear: 1975,
    },
  ],
}
```

400: Most likely missing a parameter.  You should see the error message in the response along with what parameter is missing

200: You will get an object with property equal to each station id given and a list as the properties value of last 20 trips on the given day.

## Development

### Running development mode

Clone the repo, install the dependencies, and start the local server.

```
git clone git@gitlab.com:ggonza89/yum-node-challenge.git
npm i
npm run dev
```

### Running tests

There are api tests and a couple of unit tests.  Running them will require the service to be up and running and ready to serve.

Look out for the `listening at http://localhost:3000` console output to be sure it is ready.

Once it is ready, feel free to run the following command.

```
npm test
```
