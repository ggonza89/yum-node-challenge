const { lastTrips } = require("../src/lastTrips");

describe(`last 20 trips test`, () => {
  test(`should parse trip data correctly`, async () => {
    global.tripData = {
      "04/01/2019": [
        {
          localStartTime: "2019-04-01 00:02:22",
          localEndTime: "2019-04-01 17:09:48",
          endStationId: 59,
          birthYear: 1975,
        },
        {
          localStartTime: "2019-04-01 00:02:22",
          localEndTime: "2019-04-01 00:09:48",
          endStationId: 56,
          birthYear: 1975,
        },
        {
          localStartTime: "2019-04-01 00:03:02",
          localEndTime: "2019-04-01 00:20:30",
          endStationId: 56,
          birthYear: 1984,
        },
        {
          localStartTime: "2019-04-01 00:02:22",
          localEndTime: "2019-04-01 17:09:48",
          endStationId: 56,
          birthYear: 1975,
        },
      ],
    };
    const requestMock = {
      body: {
        date: "04/01/2019",
        stationIds: [56],
      },
    };
    const responseMock = {
      send: jest.fn(),
    };

    await lastTrips(requestMock, responseMock);

    expect(responseMock.send.mock.calls[0][0]).toEqual({
      "56": [
        {
          localStartTime: "2019-04-01 00:02:22",
          localEndTime: "2019-04-01 00:09:48",
          endStationId: 56,
          birthYear: 1975,
        },
        {
          localStartTime: "2019-04-01 00:03:02",
          localEndTime: "2019-04-01 00:20:30",
          endStationId: 56,
          birthYear: 1984,
        },
        {
          localStartTime: "2019-04-01 00:02:22",
          localEndTime: "2019-04-01 17:09:48",
          endStationId: 56,
          birthYear: 1975,
        },
      ],
    });
  });
});
