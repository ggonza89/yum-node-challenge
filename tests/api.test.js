const axios = require("axios");

describe(`api tests`, () => {
  const url = "http://localhost:5000";
  const config = {
    headers: {
      Cookie: "accessToken=1234;",
    },
  };

  test(`should fail if no token provided`, async () => {
    let noTokenError;
    try {
      await axios.post(`${url}/getStationById`);
    } catch (error) {
      noTokenError = error;
    }

    expect(noTokenError.response.status).toBe(500);
    expect(noTokenError.response.data).toContain("Must provide an API token");
  });

  test(`should get a station by id`, async () => {
    const response = await axios.post(
      `${url}/getStationById`,
      { id: "2" },
      config
    );

    expect(response.status).toBe(200);
    expect(response.data).toEqual({
      station_type: "classic",
      name: "Buckingham Fountain",
      short_name: "15541",
      electric_bike_surcharge_waiver: false,
      has_kiosk: true,
      external_id: "a3a36d9e-a135-11e9-9cda-0a87ae2ba916",
      station_id: "2",
      rental_methods: expect.anything(),
      lat: 41.87651122881695,
      eightd_has_key_dispenser: false,
      capacity: 39,
      eightd_station_services: [],
      rental_uris: {
        android: "https://chi.lft.to/lastmile_qr_scan",
        ios: "https://chi.lft.to/lastmile_qr_scan",
      },
      lon: -87.62054800987242,
    });
    expect(response.data.rental_methods.sort()).toEqual(
      ["TRANSITCARD", "CREDITCARD", "KEY"].sort()
    );
  });

  test(`should have status of 400 when no station id is provided`, async () => {
    let badRequestError;

    try {
      await axios.post(`${url}/getStationById`, {}, config);
    } catch (error) {
      badRequestError = error;
    }

    expect(badRequestError.response.status).toBe(400);
    expect(badRequestError.response.data).toContain(
      "id is a required parameter"
    );
  });

  test(`should get riders in age group`, async () => {
    const response = await axios.post(
      `${url}/ridersOnGivenDay`,
      { date: "04/01/2019", stationIds: [56] },
      config
    );

    expect(response.status).toBe(200);
    expect(response.data).toEqual([
      {
        ageGroup: "0-20",
        riders: 0,
      },
      {
        ageGroup: "21-30",
        riders: 18,
      },
      {
        ageGroup: "31-40",
        riders: 17,
      },
      {
        ageGroup: "41-50",
        riders: 5,
      },
      {
        ageGroup: "51+",
        riders: 2,
      },
      {
        ageGroup: "unknown",
        riders: 1,
      },
    ]);
  });

  test(`should have status of 400 when date is not provided`, async () => {
    let badRequestError;

    try {
      await axios.post(`${url}/ridersOnGivenDay`, {}, config);
    } catch (error) {
      badRequestError = error;
    }

    expect(badRequestError.response.status).toBe(400);
    expect(badRequestError.response.data).toContain(
      "date is a required parameter"
    );
  });

  test(`should have status of 400 when stationIds is not provided`, async () => {
    let badRequestError;

    try {
      await axios.post(
        `${url}/ridersOnGivenDay`,
        { date: "04/01/2019" },
        config
      );
    } catch (error) {
      badRequestError = error;
    }

    expect(badRequestError.response.status).toBe(400);
    expect(badRequestError.response.data).toContain(
      "stationIds is a required parameter"
    );
  });

  test(`should get last 20 trips at station and date given`, async () => {
    const response = await axios.post(
      `${url}/lastTrips`,
      { date: "04/02/2019", stationIds: [56] },
      config
    );

    expect(response.status).toBe(200);
    expect(response.data["56"].length).toBe(20);
  });

  test(`should have status of 400 when date is not provided in last trips call`, async () => {
    let badRequestError;

    try {
      await axios.post(`${url}/lastTrips`, {}, config);
    } catch (error) {
      badRequestError = error;
    }

    expect(badRequestError.response.status).toBe(400);
    expect(badRequestError.response.data).toContain(
      "date is a required parameter"
    );
  });

  test(`should have status of 400 when stationIds is not provided in last trips call`, async () => {
    let badRequestError;

    try {
      await axios.post(`${url}/lastTrips`, { date: "04/01/2019" }, config);
    } catch (error) {
      badRequestError = error;
    }

    expect(badRequestError.response.status).toBe(400);
    expect(badRequestError.response.data).toContain(
      "stationIds is a required parameter"
    );
  });
});
