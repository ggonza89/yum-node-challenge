const { findRidersInAgeGroup } = require("../src/findRidersInAgeGroup");

describe(`find riders in age group`, () => {
  test(`find riders in age group unknown`, async () => {
    const riders = findRidersInAgeGroup(trips, 2019);

    expect(riders).toBe(1);
  });

  test(`find riders in age group 0-20`, async () => {
    const riders = findRidersInAgeGroup(trips, 2019, 0, 20);

    expect(riders).toBe(0);
  });

  test(`find riders in age group 21-30`, async () => {
    const riders = findRidersInAgeGroup(trips, 2019, 21, 30);

    expect(riders).toBe(18);
  });

  test(`find riders in age group 31-40`, async () => {
    const riders = findRidersInAgeGroup(trips, 2019, 31, 40);

    expect(riders).toBe(17);
  });

  test(`find riders in age group 41-50`, async () => {
    const riders = findRidersInAgeGroup(trips, 2019, 41, 50);

    expect(riders).toBe(5);
  });

  test(`find riders in age group 51+`, async () => {
    const riders = findRidersInAgeGroup(trips, 2019, 51, 1000);

    expect(riders).toBe(2);
  });
});

const trips = [
  {
    localStartTime: "2019-04-01 00:02:22",
    localEndTime: "2019-04-01 00:09:48",
    endStationId: 56,
    birthYear: 1975,
  },
  {
    localStartTime: "2019-04-01 06:32:52",
    localEndTime: "2019-04-01 06:46:38",
    endStationId: 56,
    birthYear: 1981,
  },
  {
    localStartTime: "2019-04-01 07:00:55",
    localEndTime: "2019-04-01 07:03:50",
    endStationId: 56,
    birthYear: 1988,
  },
  {
    localStartTime: "2019-04-01 08:10:15",
    localEndTime: "2019-04-01 08:15:40",
    endStationId: 56,
    birthYear: 1990,
  },
  {
    localStartTime: "2019-04-01 08:26:22",
    localEndTime: "2019-04-01 08:39:04",
    endStationId: 56,
    birthYear: 1971,
  },
  {
    localStartTime: "2019-04-01 08:43:21",
    localEndTime: "2019-04-01 09:12:23",
    endStationId: 56,
    birthYear: 1988,
  },
  {
    localStartTime: "2019-04-01 09:15:48",
    localEndTime: "2019-04-01 09:30:45",
    endStationId: 56,
    birthYear: null,
  },
  {
    localStartTime: "2019-04-01 10:26:14",
    localEndTime: "2019-04-01 10:32:21",
    endStationId: 56,
    birthYear: 1985,
  },
  {
    localStartTime: "2019-04-01 10:43:37",
    localEndTime: "2019-04-01 10:47:35",
    endStationId: 56,
    birthYear: 1987,
  },
  {
    localStartTime: "2019-04-01 10:44:48",
    localEndTime: "2019-04-01 10:59:10",
    endStationId: 56,
    birthYear: 1979,
  },
  {
    localStartTime: "2019-04-01 10:56:50",
    localEndTime: "2019-04-01 10:59:10",
    endStationId: 56,
    birthYear: 1986,
  },
  {
    localStartTime: "2019-04-01 12:07:13",
    localEndTime: "2019-04-01 12:09:15",
    endStationId: 56,
    birthYear: 1955,
  },
  {
    localStartTime: "2019-04-01 15:44:38",
    localEndTime: "2019-04-01 15:55:15",
    endStationId: 56,
    birthYear: 1963,
  },
  {
    localStartTime: "2019-04-01 16:09:33",
    localEndTime: "2019-04-01 16:20:05",
    endStationId: 56,
    birthYear: 1985,
  },
  {
    localStartTime: "2019-04-01 16:52:05",
    localEndTime: "2019-04-01 17:00:22",
    endStationId: 56,
    birthYear: 1992,
  },
  {
    localStartTime: "2019-04-01 16:57:08",
    localEndTime: "2019-04-01 17:08:26",
    endStationId: 56,
    birthYear: 1985,
  },
  {
    localStartTime: "2019-04-01 16:57:10",
    localEndTime: "2019-04-01 17:07:16",
    endStationId: 56,
    birthYear: 1988,
  },
  {
    localStartTime: "2019-04-01 17:01:32",
    localEndTime: "2019-04-01 17:07:16",
    endStationId: 56,
    birthYear: 1973,
  },
  {
    localStartTime: "2019-04-01 17:01:38",
    localEndTime: "2019-04-01 17:10:56",
    endStationId: 56,
    birthYear: 1990,
  },
  {
    localStartTime: "2019-04-01 17:05:58",
    localEndTime: "2019-04-01 17:14:38",
    endStationId: 56,
    birthYear: 1980,
  },
  {
    localStartTime: "2019-04-01 17:06:15",
    localEndTime: "2019-04-01 17:14:13",
    endStationId: 56,
    birthYear: 1990,
  },
  {
    localStartTime: "2019-04-01 17:10:36",
    localEndTime: "2019-04-01 17:16:45",
    endStationId: 56,
    birthYear: 1992,
  },
  {
    localStartTime: "2019-04-01 17:11:29",
    localEndTime: "2019-04-01 17:17:01",
    endStationId: 56,
    birthYear: 1990,
  },
  {
    localStartTime: "2019-04-01 17:16:12",
    localEndTime: "2019-04-01 17:22:55",
    endStationId: 56,
    birthYear: 1993,
  },
  {
    localStartTime: "2019-04-01 17:20:45",
    localEndTime: "2019-04-01 17:24:40",
    endStationId: 56,
    birthYear: 1993,
  },
  {
    localStartTime: "2019-04-01 17:22:53",
    localEndTime: "2019-04-01 17:33:14",
    endStationId: 56,
    birthYear: 1992,
  },
  {
    localStartTime: "2019-04-01 17:25:37",
    localEndTime: "2019-04-01 17:36:51",
    endStationId: 56,
    birthYear: 1989,
  },
  {
    localStartTime: "2019-04-01 17:40:55",
    localEndTime: "2019-04-01 17:47:35",
    endStationId: 56,
    birthYear: 1985,
  },
  {
    localStartTime: "2019-04-01 17:47:35",
    localEndTime: "2019-04-01 17:55:56",
    endStationId: 56,
    birthYear: 1989,
  },
  {
    localStartTime: "2019-04-01 17:54:17",
    localEndTime: "2019-04-01 18:03:19",
    endStationId: 56,
    birthYear: 1987,
  },
  {
    localStartTime: "2019-04-01 17:55:59",
    localEndTime: "2019-04-01 18:00:04",
    endStationId: 56,
    birthYear: 1990,
  },
  {
    localStartTime: "2019-04-01 17:57:13",
    localEndTime: "2019-04-01 18:07:05",
    endStationId: 56,
    birthYear: 1994,
  },
  {
    localStartTime: "2019-04-01 17:59:16",
    localEndTime: "2019-04-01 18:03:00",
    endStationId: 56,
    birthYear: 1989,
  },
  {
    localStartTime: "2019-04-01 18:01:58",
    localEndTime: "2019-04-01 18:06:07",
    endStationId: 56,
    birthYear: 1991,
  },
  {
    localStartTime: "2019-04-01 18:01:54",
    localEndTime: "2019-04-01 18:08:08",
    endStationId: 56,
    birthYear: 1986,
  },
  {
    localStartTime: "2019-04-01 18:11:22",
    localEndTime: "2019-04-01 18:22:46",
    endStationId: 56,
    birthYear: 1976,
  },
  {
    localStartTime: "2019-04-01 18:21:01",
    localEndTime: "2019-04-01 18:30:35",
    endStationId: 56,
    birthYear: 1981,
  },
  {
    localStartTime: "2019-04-01 18:31:36",
    localEndTime: "2019-04-01 18:35:48",
    endStationId: 56,
    birthYear: 1987,
  },
  {
    localStartTime: "2019-04-01 18:40:17",
    localEndTime: "2019-04-01 18:43:34",
    endStationId: 56,
    birthYear: 1995,
  },
  {
    localStartTime: "2019-04-01 18:51:06",
    localEndTime: "2019-04-01 19:00:36",
    endStationId: 56,
    birthYear: 1984,
  },
  {
    localStartTime: "2019-04-01 19:04:10",
    localEndTime: "2019-04-01 19:16:45",
    endStationId: 56,
    birthYear: 1989,
  },
  {
    localStartTime: "2019-04-01 19:12:49",
    localEndTime: "2019-04-01 19:15:13",
    endStationId: 56,
    birthYear: 1980,
  },
  {
    localStartTime: "2019-04-01 19:37:48",
    localEndTime: "2019-04-01 19:41:26",
    endStationId: 56,
    birthYear: 1989,
  },
  {
    localStartTime: "2019-04-01 19:57:20",
    localEndTime: "2019-04-01 20:04:02",
    endStationId: 56,
    birthYear: 1980,
  },
  {
    localStartTime: "2019-04-01 21:15:44",
    localEndTime: "2019-04-01 21:18:08",
    endStationId: 56,
    birthYear: 1986,
  },
  {
    localStartTime: "2019-04-01 21:51:05",
    localEndTime: "2019-04-01 22:00:26",
    endStationId: 56,
    birthYear: 1975,
  },
];
