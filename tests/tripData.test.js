const { parseTripData } = require("../src/parseTripData");

describe(`trip data tests`, () => {
  test(`should parse trip data correctly`, async () => {
    const tripData = await parseTripData(
      `${process.cwd()}/tests/testDivvyTripData`
    );

    expect(tripData["04/01/2019"]).toEqual([
      {
        localStartTime: "2019-04-01 00:02:22",
        localEndTime: "2019-04-01 00:09:48",
        endStationId: 56,
        birthYear: 1975,
      },
      {
        localStartTime: "2019-04-01 00:03:02",
        localEndTime: "2019-04-01 00:20:30",
        endStationId: 59,
        birthYear: 1984,
      },
      {
        localStartTime: "2019-04-01 00:11:07",
        localEndTime: "2019-04-01 00:15:19",
        endStationId: 174,
        birthYear: 1990,
      },
    ]);
  });
});
