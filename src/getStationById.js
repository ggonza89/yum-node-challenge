module.exports = {
  getStationById: async (request, response) => {
    const { id } = request.body;

    if (id) {
      const station = stationInformation.stations.filter(
        (station) => station.station_id === id
      );

      if (station.length === 1) {
        response.send(station[0]);
      } else {
        response.status(404).send(`could not find station id '${id}'`);
      }
    } else {
      response.status(400).send("id is a required parameter");
    }
  },
};
