module.exports = {
  getTripsByDateAndStationIds: (date, stationIds) => {
    let trips = global.tripData[date];

    return trips.filter((trip) => stationIds.includes(trip.endStationId));
  },
};
