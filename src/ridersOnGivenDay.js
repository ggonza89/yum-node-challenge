const {
  getTripsByDateAndStationIds,
} = require("./getTripsByDateAndStationIds");
const { findRidersInAgeGroup } = require("./findRidersInAgeGroup");
const moment = require("moment");

module.exports = {
  ridersOnGivenDay: async (request, response) => {
    const { date, stationIds } = request.body;

    if (!date) {
      response.status(400).send("date is a required parameter");
    } else if (!stationIds || stationIds.length === 0) {
      response.status(400).send("stationIds is a required parameter");
    } else {
      const trips = getTripsByDateAndStationIds(date, stationIds, response);

      if (trips.length > 0) {
        const givenYear = moment(date, "DD/MM/YYYY").year();

        const ridersOnGivenDay = [
          {
            ageGroup: "0-20",
            riders: findRidersInAgeGroup(trips, givenYear, 0, 20),
          },
          {
            ageGroup: "21-30",
            riders: findRidersInAgeGroup(trips, givenYear, 21, 30),
          },
          {
            ageGroup: "31-40",
            riders: findRidersInAgeGroup(trips, givenYear, 31, 40),
          },
          {
            ageGroup: "41-50",
            riders: findRidersInAgeGroup(trips, givenYear, 41, 50),
          },
          {
            ageGroup: "51+",
            riders: findRidersInAgeGroup(trips, givenYear, 51, 1000),
          },
          {
            ageGroup: "unknown",
            riders: findRidersInAgeGroup(trips, givenYear),
          },
        ];

        response.send(ridersOnGivenDay);
      }
    }
  },
};
