const moment = require("moment");

const csv = require("csvtojson");

const readCsv = async (fileName) => {
  return new Promise((resolve, reject) => {
    csv({
      noheader: true,
      output: "csv",
    })
      .fromFile(fileName)
      .then((csvData) => {
        const tripData = csvData.map((trip) => {
          return {
            localStartTime: trip[1],
            localEndTime: trip[2],
            endStationId: parseInt(trip[7]),
            birthYear: trip[11] ? parseInt(trip[11]) : null,
          };
        });
        resolve(tripData);
      });
  });
};

const optimizeTripData = (tripData) => {
  let dateHashMap = {};

  for (let trip of tripData) {
    const formattedLocalEndTime = moment(trip.localEndTime).format(
      "MM/DD/YYYY"
    );

    if (!dateHashMap[formattedLocalEndTime]) {
      dateHashMap[formattedLocalEndTime] = [];
    }

    dateHashMap[formattedLocalEndTime].push(trip);
  }

  return dateHashMap;
};

module.exports = {
  parseTripData: async (fileName) => {
    const tripData = await readCsv(fileName);

    return optimizeTripData(tripData);
  },
};
