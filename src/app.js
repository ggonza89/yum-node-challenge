const express = require("express");
const app = express();
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const port = 3000;
const { get } = require("lodash");
const axios = require("axios");
const { parseTripData } = require("./parseTripData");
const { lastTrips } = require("./lastTrips");
const { ridersOnGivenDay } = require("./ridersOnGivenDay");
const { getStationById } = require("./getStationById");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

const preAuth = (request, response, next) => {
  const accessToken = get(request.cookies, "accessToken");

  if (accessToken) {
    next();
  } else {
    next(new Error("Must provide an API token"));
  }
};

app.post("/getStationById", preAuth, getStationById);

app.post("/ridersOnGivenDay", preAuth, ridersOnGivenDay);

app.post("/lastTrips", preAuth, lastTrips);

const getAllData = async () => {
  const response = await axios.get(
    `https://gbfs.divvybikes.com/gbfs/en/station_information.json`
  );

  global.stationInformation = response.data.data;
  global.tripData = await parseTripData(
    `${process.cwd()}/src/Divvy_Trips_2019_Q2.csv`
  );
};

getAllData().then(() =>
  app.listen(port, () => {
    console.log(`listening at http://localhost:${port}`);
  })
);
