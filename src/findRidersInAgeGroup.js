module.exports = {
  findRidersInAgeGroup: (trips, givenYear, startRange, endRange) => {
    let ridersInAgeGroup = [];

    if (startRange >= 0 && endRange >= 0) {
      const startYear = givenYear - startRange;
      const endYear = givenYear - endRange;

      ridersInAgeGroup = trips.filter(
        (trip) =>
          trip.birthYear !== null &&
          trip.birthYear < startYear &&
          trip.birthYear >= endYear
      );
    } else {
      ridersInAgeGroup = trips.filter((trip) => trip.birthYear === null);
    }

    return ridersInAgeGroup.length;
  },
};
