const {
  getTripsByDateAndStationIds,
} = require("./getTripsByDateAndStationIds");
const moment = require("moment");

module.exports = {
  lastTrips: async (request, response) => {
    const { date, stationIds } = request.body;

    if (!date) {
      response.status(400).send("date is a required parameter");
    } else if (!stationIds || stationIds.length === 0) {
      response.status(400).send("stationIds is a required parameter");
    } else {
      const trips = getTripsByDateAndStationIds(date, stationIds, response);

      const sortedByTimeTrips = trips.sort(
        (tripA, tripB) =>
          new moment(tripA.localEndTime).format("MM/DD/YYYY") -
          new moment(tripB.localEndTime)
      );

      let stationHashMap = {};

      for (let trip of sortedByTimeTrips) {
        const stationId = `${trip.endStationId}`;

        if (!stationHashMap[stationId]) {
          stationHashMap[stationId] = [];
        }

        if (stationHashMap[stationId].length < 20) {
          stationHashMap[stationId].push(trip);
        }
      }

      response.send(stationHashMap);
    }
  },
};
